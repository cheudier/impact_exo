<?php
require __DIR__ .'/vendor/autoload.php';
use App\EventDigester\EventDigester;

if(isset($_POST['data']) && !empty($_POST['data'])) {
    try {
        $eventDigester = new EventDigester();

        // decode & format data
        $events = json_decode($_POST['data'], true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception(json_last_error_msg());
        }

        $eventsFormated = $eventDigester->formatData($events);
        $eventsFormatedWithC = $eventDigester->addConstant($eventsFormated);

        // generate csv file
        $fileNameCsv = 'export_' . microtime(true) . '.csv';
        $eventDigester->toCsv($fileNameCsv, $eventsFormatedWithC, array_merge(['heure'], EventDigester::$eventNames, ['c']));

        // send csv file
        $file = fopen($fileNameCsv, "r");
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$fileNameCsv.'";');
        // make php send the generated csv lines to the browser
        fpassthru($file);

    } catch (Exception $e) {
        header("HTTP/1.1 400 Bad Request");
        echo "Empty or malformed data => ";
        echo $e->getCode() . ':' . $e->getMessage();
    }
} else{
    header("HTTP/1.1 400 Bad Request");
    echo "Missing post parameter 'data'";
}

exit();