<?php declare(strict_types=1);

date_default_timezone_set('Etc/GMT-1');

use PHPUnit\Framework\TestCase;
use App\EventDigester\EventDigester;

class EventDigesterTest extends TestCase
{
    /**
     * @dataProvider eventsDataProvider
     */
    public function testFormatData($events, $expectedResult): void {
        $eventDigester = new EventDigester();
        $data = $eventDigester->formatData($events);
        $this->assertEquals($data, $expectedResult);
    }

    /**
     * @dataProvider eventsDataProviderWithConst
     */
    public function testFormatDataWithConst($events, $expectedResult): void {
        $eventDigester = new EventDigester();
        $data =  $eventDigester->addConstant($eventDigester->formatData($events));

        $this->assertEquals($data, $expectedResult);
    }

    /**
     * @dataProvider eventsDataProviderUniqueID
     */
    public function testFormatDataUnique($events, $expectedResult): void {
        $eventDigester = new EventDigester();
        $data =  $eventDigester->getUniqueIds($events);

        $this->assertEquals($data, $expectedResult);
    }

    public function eventsDataProvider()
    {
        return [
            'empty events' => [
                [],
                [],
            ],
            'one impression at 12h' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1341314216",
                    ],
                ],
                [
                    [
                        'heure' => '12',
                        'impressions' => 1,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                    ]
                ],
            ],
            'several impression at 12h and 00h' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1617966000",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1618009200",
                    ],
                ],
                [
                    [
                        'heure' => '00',
                        'impressions' => 1,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                    ],
                    [
                        'heure' => '12',
                        'impressions' => 3,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                    ],
                ],
            ],
            '1 event of all type same hour' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_start",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_25",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_50",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_75",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_completed",
                        "timestamp" => "1619437514",
                    ],
                ],
                [
                    [
                        'heure' => '12',
                        'impressions' => 1,
                        'video_start' => 1,
                        'video_25' => 1,
                        'video_50' => 1,
                        'video_75' => 1,
                        'video_completed' => 1,
                    ],
                ],
            ],
        ];
    }

    public function eventsDataProviderWithConst()
    {
        return [
            'empty events' => [
                [],
                [],
            ],
            'one impression at 12h' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1341314216",
                    ],
                ],
                [
                    [
                        'heure' => '12',
                        'impressions' => 1,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                        'c' => 0.1,
                    ]
                ],
            ],
            'several impression at 12h and 00h' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1617966000",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1618009250",
                    ],
                ],
                [
                    [
                        'heure' => '00',
                        'impressions' => 1,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                        'c' => 0.1,
                    ],
                    [
                        'heure' => '12',
                        'impressions' => 3,
                        'video_start' => 0,
                        'video_25' => 0,
                        'video_50' => 0,
                        'video_75' => 0,
                        'video_completed' => 0,
                        'c' => 0.3,
                    ],
                ],
            ],
            '1 event of all type same hour' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_start",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_25",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_50",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_75",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_completed",
                        "timestamp" => "1619437514",
                    ],
                ],
                [
                    [
                        'heure' => '12',
                        'impressions' => 1,
                        'video_start' => 1,
                        'video_25' => 1,
                        'video_50' => 1,
                        'video_75' => 1,
                        'video_completed' => 1,
                        'c' => 0.1,
                    ],
                ],
            ],
            ' all events but 0 impressions' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_start",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_25",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_50",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_75",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_completed",
                        "timestamp" => "1619437514",
                    ],
                ],
                [
                    [
                        'heure' => '12',
                        'impressions' => 0,
                        'video_start' => 1,
                        'video_25' => 1,
                        'video_50' => 1,
                        'video_75' => 1,
                        'video_completed' => 1,
                        'c' => 0,
                    ],
                ],
            ],
        ];
    }

    public function eventsDataProviderUniqueID()
    {
        return [
            'empty events' => [
                [],
                [],
            ],
            'one unique ID' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1341314216",
                    ],
                ],
                [
                    ['id' => 'xxxx-xxxx-xxxx-0000'],
                ],
            ],
            'only unique ids, no duplicate' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1341314216",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0001",
                        "event" => "impressions",
                        "timestamp" => "1341014216",
                    ],
                    [
                        "id" => "axxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1341314216",
                    ],
                ],
                [
                    ['id' => 'xxxx-xxxx-xxxx-0000'],
                    ['id' => 'xxxx-xxxx-xxxx-0001'],
                    ['id' => 'axxx-xxxx-xxxx-0000'],
                ],
            ],
            '3 unique ids with different status, 2 same ids' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0001",
                        "event" => "impressions",
                        "timestamp" => "1617968714",
                    ],
                    [
                        "id" => "12xx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1617966000",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1618029200",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_start",
                        "timestamp" => "1618029200",
                    ],
                ],
                [
                    ['id' => 'xxxx-xxxx-xxxx-0000'],
                    ['id' =>  'xxxx-xxxx-xxxx-0001'],
                    ['id' => '12xx-xxxx-xxxx-0000'],
                ],
            ],
            'only unique ids' => [
                [
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "impressions",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_start",
                        "timestamp" => "161796s714",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_25",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_50",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_75",
                        "timestamp" => "1619437514",
                    ],
                    [
                        "id" => "xxxx-xxxx-xxxx-0000",
                        "event" => "video_completed",
                        "timestamp" => "1619437514",
                    ],
                ],
                [
                    ['id' => 'xxxx-xxxx-xxxx-0000'],
                ],
            ],
        ];
    }
}
