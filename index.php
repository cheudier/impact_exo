<?php
date_default_timezone_set('Etc/GMT-1');

require __DIR__ .'/vendor/autoload.php';

use App\EventDigester\EventDigester;

// sample data
$jsonData = '
[
    {"id": "xxxx-xxxx-xxxx-0000", "event": "impressions", "timestamp": "1874185728"},
    {"id": "xxxx-xxxx-xxxx-0001", "event": "video_start", "timestamp": "1762140084"},
    {"id": "xxxx-xxxx-xxxx-0002", "event": "video_start", "timestamp": "1062118076"},
    {"id": "xxxx-xxxx-xxxx-0003", "event": "video_25", "timestamp": "1182604784"},
    {"id": "xxxx-xxxx-xxxx-0003", "event": "video_completed", "timestamp": "1168136237"},
    {"id": "xxxx-xxxx-xxxx-0003", "event": "video_completed", "timestamp": "1319204268"},
    {"id": "xxxx-xxxx-xxxx-0003", "event": "video_completed", "timestamp": "1105667578"},
    {"id": "xxxx-xxxx-xxxx-0004", "event": "video_25", "timestamp": "1762140084"},
    {"id": "xxxx-xxxx-xxxx-0005", "event": "video_25", "timestamp": "1784499778"},
    {"id": "xxxx-xxxx-xxxx-0005", "event": "video_25", "timestamp": "1784499778"},
    {"id": "xxxx-xxxx-xxxx-0005", "event": "video_25", "timestamp": "1784499778"},
    {"id": "xxxx-xxxx-xxxx-0006", "event": "video_completed", "timestamp": "1525014334"},
    {"id": "xxxx-xxxx-xxxx-0007", "event": "video_completed", "timestamp": "1634590961"},
    {"id": "xxxx-xxxx-xxxx-0008", "event": "video_completed", "timestamp": "1178274164"},
    {"id": "xxxx-xxxx-xxxx-0009", "event": "video_completed", "timestamp": "1739053915"},
    {"id": "xxxx-xxxx-xxxx-0010", "event": "video_start", "timestamp": "1305260664"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0011", "event": "impressions", "timestamp": "1341314216"},
    {"id": "xxxx-xxxx-xxxx-0012", "event": "impressions", "timestamp": "1159097848"},
    {"id": "xxxx-xxxx-xxxx-0013", "event": "video_start", "timestamp": "1761150565"},
    {"id": "xxxx-xxxx-xxxx-0014", "event": "video_start", "timestamp": "1363281788"},
    {"id": "xxxx-xxxx-xxxx-0015", "event": "video_start", "timestamp": "1031924324"},
    {"id": "xxxx-xxxx-xxxx-0016", "event": "video_75", "timestamp": "1594809519"},
    {"id": "xxxx-xxxx-xxxx-0017", "event": "video_25", "timestamp": "1501156700"},
    {"id": "xxxx-xxxx-xxxx-0018", "event": "video_25", "timestamp": "1085160051"},
    {"id": "xxxx-xxxx-xxxx-0019", "event": "video_25", "timestamp": "1718359430"},
    {"id": "xxxx-xxxx-xxxx-0019", "event": "video_25", "timestamp": "1737919795"},
    {"id": "xxxx-xxxx-xxxx-0019", "event": "video_25", "timestamp": "1365059557"},
    {"id": "xxxx-xxxx-xxxx-0020", "event": "video_25", "timestamp": "1633293984"},
    {"id": "xxxx-xxxx-xxxx-0021", "event": "video_75", "timestamp": "1742165313"},
    {"id": "xxxx-xxxx-xxxx-0022", "event": "video_50", "timestamp": "1367412690"},
    {"id": "xxxx-xxxx-xxxx-0023", "event": "video_start", "timestamp": "1127505010"},
    {"id": "xxxx-xxxx-xxxx-0024", "event": "video_75", "timestamp": "1304681908"},
    {"id": "xxxx-xxxx-xxxx-0025", "event": "video_50", "timestamp": "1011261218"},
    {"id": "xxxx-xxxx-xxxx-0026", "event": "video_50", "timestamp": "1497214398"},
    {"id": "xxxx-xxxx-xxxx-0026", "event": "video_50", "timestamp": "1497214398"},
    {"id": "xxxx-xxxx-xxxx-0026", "event": "video_50", "timestamp": "1497214398"},
    {"id": "xxxx-xxxx-xxxx-0027", "event": "video_75", "timestamp": "1620284774"},
    {"id": "xxxx-xxxx-xxxx-0028", "event": "video_50", "timestamp": "1824621941"},
    {"id": "xxxx-xxxx-xxxx-0029", "event": "video_start", "timestamp": "1421467218"},
    {"id": "xxxx-xxxx-xxxx-0030", "event": "video_50", "timestamp": "1554544342"}
]
';

// json est le format le plus ressemblant à un 'dict'
$eventsArr = json_decode($jsonData, true);
$eventDigester = new EventDigester();

// 1 digest data and index status by hour
$eventsFormated = $eventDigester->formatData($eventsArr);
$eventDigester->displayArray($eventsFormated, array_merge(['heure'], EventDigester::$eventNames));

// 2 add new index by hour : 'c' = number of impressions * 0.1
$eventsWithConst = $eventDigester->addConstant($eventsFormated);
$eventDigester->displayArray($eventsWithConst, array_merge(['heure', 'c'], EventDigester::$eventNames));

// 3 write data in csv file
$eventDigester->toCsv('export.csv', $eventsWithConst,array_merge(['heure', 'c'], EventDigester::$eventNames));

// 4 display unique ids
$uniqueIds = $eventDigester->getUniqueIds($eventsArr);
$eventDigester->displayArray($uniqueIds,['id']);

