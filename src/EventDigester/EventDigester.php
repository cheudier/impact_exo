<?php

namespace App\EventDigester;

use App\lib\EasyTable;


class EventDigester
{
    private const c = 0.1;

    static array $eventNames = [
        'impressions',
        'video_start',
        'video_25',
        'video_50',
        'video_75',
        'video_completed'
    ];

    /**
     * @param $dataArr
     * @param array $headers
     */
    public function displayArray($dataArr, array $headers = []): void {
        $table = new EasyTable();
        $table->setHead($headers, 'th_blue');
        $table->setRows($dataArr);

        echo $table;
    }

    /**
     * @param array $eventsArr
     * @return array
     */
    public function formatData(array $eventsArr): array {
        $formatedData =[];
        foreach($eventsArr as $event) {
            $hour = date('H', $event['timestamp']);

            $currentEvent = $event['event'];

            if(!isset($formatedData[$hour])) {
                $formatedData[$hour] = [
                    'heure' => $hour,
                    'impressions' => 0,
                    'video_start' => 0,
                    'video_25' => 0,
                    'video_50' => 0,
                    'video_75' => 0,
                    'video_completed' => 0,
                ];
            }
            $formatedData[$hour][$currentEvent]++;
        }
        sort($formatedData);
        return $formatedData;
    }

    /**
     * @param $dataArr
     * @return array
     */
    public function addConstant($dataArr): array {
        array_walk($dataArr, function(&$row) {
            $row['c'] = isset($row['impressions'])
                ? $row['impressions'] * self::c
                : 0;
        });
        return $dataArr;
    }

    /**
     *
     * TODO : this way ius doing a lot of I/O to disk. It could be quicker to write a large amount of lines, depending of ram available
     *
     * @param string $name
     * @param array $dataArr
     * @param array $headers
     */
    public function toCsv(string $name, array $dataArr, array $headers = []): void {
        $fp = fopen($name, 'w+');

        if(!empty($headers)) {
            fputcsv($fp, $headers);
        }

        foreach ($dataArr as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    /**
     * @param array $events
     * @return array
     */
    public function getUniqueIds(array $events): array {
        $ids = array_column($events, 'id');
        $uniqueIds = array_unique($ids, SORT_STRING);
        array_walk($uniqueIds, function(&$row) {
            $row = ['id' => $row];
        });
        return $uniqueIds;
    }
}