# Usage

## install

### requirements :
- docker and pref non root user : https://docs.docker.com/engine/install/linux-postinstall/
- to avoid errors on writing csv files, run this in home folder subdirectories

### installation of dependencies (PHPUnit)
In root project :

`docker run --rm --interactive  --user $(id -u):$(id -g) --tty --volume $PWD:/app composer install`

## run server
In root project :

`docker run --rm -p 8080:80  --user $(id -u):$(id -g) --name exo-php -v "$PWD":/var/www/html php:8-apache`

[localhost:8080](http://localhost:8080 "localhost")

## tests with phpunit

`docker run -it --rm --name impact-test -v "$PWD":/usr/src/myapp -w /usr/src/myapp php:8-cli ./vendor/bin/phpunit tests`

See file `tests/EventDigesterTests.php`

## API

If you do not have the server running, see first step **run server**

[localhost:8080/api.php](http://localhost:8080/api.php "localhost api") with POST dict in parameter 'data'
